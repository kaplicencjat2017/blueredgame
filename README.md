Bluered - 

Nasz projekt BlueRed, jest to kooperacyjna gra logiczna, w której gracze muszą połączyć siły, aby przejść przez labirynt i wydostać się ostatecznie na wolność. Gracze sterują dwoma żywiołakami: wodą lub ogniem (Aquanna lub Sparker). Gracze wchodzą w interakcję z elementami gry, takimi jak dźwignie, przyciski, ruchome obiekty oraz zbiorniki z wodą i lawą. Aquanna może przebywać w wodzie, ginie zaś w kontakcie z lawą. Sparker odwrotnie. Zagadki w większości będą polegały na połączeniu mocy obu żywiołów i pokonaniu w ten sposób kolejnych trudności. Gracze mogą komunikować się w grze za pomocą oznaczania różnych elementów poziomu.

Paweł Luszuk : 

- implementacja gracza : sterowanie, poruszanie się postaci, synchronizacja przez sieć
- prototypowanie oraz implementacja poziomów : rzeźbienie terenu, ustawianie  roślinności, tworzenie zagadek, level design
- animacje robione w Unity
- implementacja dźwięków oraz muzyki
- implementacja  AnimationObject
- implementacja lobby gry
- implementacja animacji w grze

Kacper Narożnik : 

- logowanie/rejestracja : napisanie skryptów PHP, podłączenie bazy danych.
- implementacja mechanik w poziomach 3D - implementacja płyt naciskowych, drzwi, dźwigni, obiektów do podnoszenia, poruszających się obiektów
- Tutorial - tutorial object, wyświetlanie pomocniczych tekstów.
- implementacja PlayerSetup - rozróżnianie który gracz jest lokalnym graczem. Włączanie i wyłączanie skryptów z zależności od tego.
przejścia między poziomami - ściemnianie i rozjaśnianie ekranu, przejścia na każdy poziom (gry oraz menu)
- struktura redBlueObject - cała struktura dziedziczenia aby jak najbardziej ograniczyć ilość kodu oraz wszystkie klasy dziedziczące

Adam Makiewicz :

- Modele postaci z animacjami
- Modele otoczenia
- Shadery
- Materiały i Texturowanie
- Graficzny interfejs użytkownika
- Efekty wizualne
- Wskaźnik myszy
- Efekty cząsteczkowe
- Poświaty
- Światło na scenie
- Dźwięki
- Dopracowywanie poziomów
- Testowanie
 
Założenia : 

Gracze startują na początku poziomu. Jeśli chociaż jeden z nich zginie cofają sie na początek poziomu. Gracze starują żywiołakiem ognia i wody, które muszą opuścić laboratorium.

Zagrożenia : ogień, woda, bagno. 

Przeszkody : przyciski + platformy, później dzwignie, ciężarki, grawitacja.

Doc : https://docs.google.com/document/d/1GYfEW1X9VKL4SQ7s-UHkrKUE7w3L2c-QL1CKAPW4EPc/edit

Dokumentacja : https://docs.google.com/document/d/13e41vz_xMBX6gyBvxLqD2KnvRrHj1whU3t4FRZyjAxM/edit?usp=sharing

MapClass : https://www.draw.io/#G0B7UjVGc5rWPQX1NSaVVFV3NoVVU