﻿using UnityEngine;
using System.Collections;

public class AudioZone : MonoBehaviour {

    private string theCollider;

    void OnTriggerEnter(Collider other)
    {
        theCollider = other.tag;
        if (theCollider == "Player")
        {
            this.GetComponent<AudioSource>().Play();
            this.GetComponent<AudioSource>().loop = true;
            Debug.Log("play ");
        }
    }
    void OnTriggerExit(Collider other)
    {
        theCollider = other.tag;
        if (theCollider == "Player")
        {
            Debug.Log("play ");
            this.GetComponent<AudioSource>().Stop();
        }
    }
}
