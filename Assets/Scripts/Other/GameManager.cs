using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{

    public static GameManager instance;

    [SerializeField]private GameObject sceneCamera;

	public int PlayersInEnd = 0;

	public GameObject WhatToDoAfterLevelPanel;
	public GameObject pingArrow;

    void Awake(){
        if (instance != null){
            Debug.LogError("More than one GameManager in scene.");
        }else{
            instance = this;
        }
    }

    void Start(){
		MouseFollow.singleton.changeVisible (false);
    }
		
    public void SetSceneCameraActive(bool isActive){

        if (sceneCamera == null)
            return;

        sceneCamera.SetActive(isActive);
    }

	#region Ending trigger functions 

	public void Quit(){
		Application.Quit ();
	}

	public void GoToNextLevel(string nextlevelName){
		foreach (PlayerInteract interact in FindObjectsOfType<PlayerInteract>()) {
			if (interact.isServer && interact.isLocalPlayer) {
				interact.CmdStartGame (nextlevelName);
			}
		}
	}

	public void ChangeElements(){
		foreach (PlayerInteract interact in FindObjectsOfType<PlayerInteract>()) {
			if (interact.isServer && interact.isLocalPlayer) {
				interact.CmdStartGame ("CharacterSelect");
			}
		}
		//NetworkManager.singleton.ServerChangeScene("CharacterSelect");
	}

	public void GoToMainMenu(){
		SceneManager.LoadScene (1);
	}

	#endregion
}
