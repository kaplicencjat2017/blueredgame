﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Util{

	public static int GetIndexOfObjectFromDictonary<T,V>(Dictionary<T, V> dic, T value){
		T[] keys = dic.Keys.ToArray ();
		for (int i = 0; i < keys.Length; i++) {
			if (keys [i].Equals(value)) {
				return i;
			}
		}
		return -1;
	}

	public static string IntToTag(int input){
		if (input == 0) {
			return "Fire";
		} else {
			return "Water";
		}
	}
}
