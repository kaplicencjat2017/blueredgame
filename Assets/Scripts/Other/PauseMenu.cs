using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	public static bool IsOn = false;
	private NetworkManager networkManager;

	void Start ()
	{
		networkManager = NetworkManager.singleton;
	}

	public void LeaveRoom ()
	{
		if (!FindObjectOfType<CameraOnMenuStart> ().blackOutRunning) {
			//StartCoroutine (FindObjectOfType<CameraOnMenuStart> ().BlackOut ());
			StartCoroutine(FindObjectOfType<CameraOnMenuStart> ().changeScreen(Color.clear, Color.black, true));
			StartCoroutine (CheckIfNow (FindObjectOfType<CameraOnMenuStart> ()));
		}

    }

	private IEnumerator CheckIfNow(CameraOnMenuStart camera){
		FindObjectOfType<PlayerUI> ().ChangeForEndOfLevel ();
		while (camera.blackOutRunning) {
			yield return new WaitForEndOfFrame ();
		}

		PlayerPrefs.SetInt ("Blackout", 1);
		MatchInfo matchInfo = networkManager.matchInfo;
		networkManager.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, networkManager.OnDropConnection);
		networkManager.StopHost();
	}

}
