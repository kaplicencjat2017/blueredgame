﻿using UnityEngine;
using System.Collections;

public class RotatingObject : MonoBehaviour {

	private bool rotating = false;

	public void RotateFunction(){
		if (!rotating) {
			StartCoroutine (Rotate ());
			rotating = true;
		}
	}
	public void StopRotation(){
		rotating = false;
		StopAllCoroutines ();
	}

	IEnumerator Rotate(){
		while (true) {
			transform.Rotate (new Vector3 (0, 180, 0), 40 * Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
	}
}
