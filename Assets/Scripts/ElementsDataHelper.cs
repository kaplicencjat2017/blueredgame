﻿using UnityEngine;
using System.Collections;

public class ElementsDataHelper : MonoBehaviour {

	public static ElementsDataHelper singleton;

	public string FireName;
	public string WaterName;

	void Awake ()
	{
		if (singleton != null)
		{
			Destroy(gameObject);
			return;
		}

		singleton = this;

		DontDestroyOnLoad(this);
	}
}
