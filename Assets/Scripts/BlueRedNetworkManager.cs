﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class BlueRedNetworkManager : NetworkManager {

//	To symuluje zachowanie jak autoCreatePlayer
//	public override void OnClientSceneChanged (NetworkConnection conn)
//	{
//		ClientScene.AddPlayer(client.connection, 0);
//	}

	public override void ServerChangeScene(string newSceneName)
	{
		SceneManager.LoadScene(newSceneName);
		base.ServerChangeScene(newSceneName);
	}
}
