﻿using UnityEngine;
using System.Collections;

public class TutorialObject : MonoBehaviour, ITrigger {

	public string title;
	[TextArea(2,10)]
	public string context;

	public void OnTriggerEnter(Collider other){
		if (other.tag == "Player" && other.gameObject.GetComponent<Player> ().isLocalPlayer) {
			TutorialManager.singleton.ShowMessage (title, context);
		}
	}

	public void OnTriggerStay(Collider other){

	}


	public void OnTriggerExit(Collider other){
		if (other.tag == "Player" && other.gameObject.GetComponent<Player> ().isLocalPlayer) {
			TutorialManager.singleton.ShowMessage ("", "");
		}
	}

}
