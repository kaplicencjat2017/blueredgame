﻿using UnityEngine;
using System.Collections;

public class StartTutorialTrigger : MonoBehaviour, ITrigger
{

    private bool readed = false;

    public void OnTriggerEnter(Collider other)
    {
        if (readed)
        {
            return;
        }

        if (other.tag == "Player" && other.gameObject.GetComponent<Player>().isLocalPlayer)
        {
            if (other.gameObject.GetComponent<Player>().PlayerElement.ToString() == "FIRE")
            {
                ShowMessageForFire();
            }
            else
            {
                ShowMessageForWater();
            }

        }
    }

    void ShowMessageForWater()
    {
        TutorialManager.singleton.ShowMessage("The Great Escape?",
            "Hello it seems that you are alive! " +
            "So, you survived. Now, you must run away from" +
            " this land. Go up, and find a way. You can move by clicking buttons WSAD on keyboard. " +
                "Look after your Brother ! Now GO !");
    }

    void ShowMessageForFire()
    {
        TutorialManager.singleton.ShowMessage("The Great Escape?",
            "Hello it seems that you are alive! " +
            "So, you survived. Now, you must run away from" +
            " this land. Go up, and find a way. You can move by clicking buttons WSAD on keyboard. " +
            "Look after your Sister ! Now GO !");
    }

    public void OnTriggerStay(Collider other)
    {

    }


    public void OnTriggerExit(Collider other)
    {
        TutorialManager.singleton.ShowMessage("", "");
        readed = true;
    }

}
