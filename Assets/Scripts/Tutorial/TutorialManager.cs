﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {

	public static TutorialManager singleton;
	public Transform TutorialShower;

	private Text title;
	private Text context;

	void Awake(){
		singleton = this;
	}

	void Start () {
		title = TutorialShower.GetChild (0).GetComponent<Text> ();
		context = TutorialShower.GetChild (1).GetComponent<Text> ();
	}

	public void ShowMessage(string _title, string _context){
		title.text = _title;
		context.text = _context;
	}
}
