﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationObject : MonoBehaviour {

	Animation my_Anim;

	AnimationClip curr_Anim;
	int curr_Anim_count;
	int max_clip_count;

    public List<AnimationClip> piwots = new List<AnimationClip>();

	// Use this for initialization
	void Start () {
		my_Anim = GetComponent<Animation> ();
		curr_Anim_count = 0;
		max_clip_count = my_Anim.GetClipCount ();
		curr_Anim = GetClipByIndex (curr_Anim_count);
	}
	
	public void LEVER_ChangeState(){
		if (curr_Anim_count >=  max_clip_count) curr_Anim_count = 0;

        GetComponent<AudioSource>().Play();
        // wtf 
        Debug.Log("anim :: " + curr_Anim_count);
        my_Anim.Play(piwots[curr_Anim_count].name);
        curr_Anim_count++;
        //curr_Anim = GetClipByIndex (curr_Anim_count);
    }

    void Update()
    {
        if(!my_Anim.isPlaying) GetComponent<AudioSource>().Stop();
    }

    AnimationClip GetClipByIndex(int index){
		int i = 0;
		foreach (AnimationState animationState in my_Anim){
			if (i == index) {
				return animationState.clip;
			}
			i++;
		}
		return null;
	}
}
