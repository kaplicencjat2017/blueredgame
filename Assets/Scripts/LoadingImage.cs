﻿using UnityEngine;
using System.Collections;

public class LoadingImage : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		if (gameObject.activeInHierarchy) {
			this.transform.Rotate(new Vector3(0f, 0f, 10f));
		}
	}
}
