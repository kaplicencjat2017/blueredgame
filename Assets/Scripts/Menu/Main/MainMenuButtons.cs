﻿using UnityEngine;
using System.Collections;

public class MainMenuButtons : MonoBehaviour
{

    public GameObject HostGamePanel;
	public GameObject JoinGameModule;
	public GameObject MenuButtonsModule;
	public GameObject AuthorsPanel;
	public GameObject OptionsPanel;

    public void SetHostGamePanelVisible()
    {
        HostGamePanel.SetActive(true);
    }

    public void SetGostGamePanelNotVisible()
    {
        HostGamePanel.SetActive(false);
    }

    public void OptionsMenu()
    {
		OptionsPanel.SetActive (true);
		AuthorsPanel.SetActive (false);
		JoinGameModule.SetActive (false);
		HostGamePanel.SetActive (false);
		MenuButtonsModule.SetActive(false);
    }

	public void AuthorsMenu()
	{
		OptionsPanel.SetActive (false);
		AuthorsPanel.SetActive (true);
		JoinGameModule.SetActive (false);
		HostGamePanel.SetActive (false);
		MenuButtonsModule.SetActive(false);
	}

	public void BackToMain(){
		OptionsPanel.SetActive (false);
		AuthorsPanel.SetActive (false);
		JoinGameModule.SetActive (true);
		MenuButtonsModule.SetActive(true);
	}

    public void QuitGame()
    {
        Application.Quit();
    }

	void Start(){
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.None;
		MouseFollow.singleton.changeVisible (true);
	}


}
