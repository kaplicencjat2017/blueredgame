﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterSelectManager : NetworkBehaviour{
    [SyncVar]
    public String WaterUsername;
    [SyncVar]
    public String FireUsername;

    Animator AquannaAnimator;
    Animator SparkerAnimator;

    [Header("Backgrounds")]
    public GameObject FireBackground;
    public GameObject WaterBackground;
    [Header("Models")]
    public GameObject AquannaGameObject;
    public GameObject SparkerGameObject;
    [Header("Texts")]
    public Text WaterText;
    public Text FireText;
    [Header("StartGameButton")]
    public Button startGameButton;
    [Header("Audio")]
    public AudioClip AquannapickAudioClip;
    public AudioClip SparkerAudioClip;

    void Start()
    {
        MouseFollow.singleton.changeVisible(true);
        AquannaAnimator = AquannaGameObject.GetComponent<Animator>();
        SparkerAnimator = SparkerGameObject.GetComponent<Animator>();

        AquannaAnimator.CrossFade("menu_idle", 0.01f);
    }

    void Update()
    {
        handleClick();
        showNames();
        handleIfCanStartGame();
    }

    void handleClick()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.transform.gameObject.tag == "Fire")
                {
                    assignNameOnServer("F", UserAccountManager.LoggedIn_Username);
                }
                else if (hit.transform.gameObject.tag == "Water")
                {
                    assignNameOnServer("W", UserAccountManager.LoggedIn_Username);
                }
            }
        }
    }

    void handleIfCanStartGame()
    {
        if (WaterText.text != "" || FireText.text != "")
        {
            startGameButton.interactable = true;
        }
        else
        {
            startGameButton.interactable = false;
        }
    }

    void showNames()
    {
        WaterText.text = WaterUsername;
        FireText.text = FireUsername;

        if (WaterUsername != ElementsDataHelper.singleton.WaterName || FireUsername != ElementsDataHelper.singleton.FireName)
        {
            synchronizeNames();
        }
    }

    void synchronizeNames()
    {
        ElementsDataHelper.singleton.WaterName = WaterUsername;
        ElementsDataHelper.singleton.FireName = FireUsername;
    }

    void assignNameOnServer(string element, string name)
    {
        foreach (PlayerForMenu player in FindObjectsOfType<PlayerForMenu>())
        {
            player.tryAssign(this.gameObject.GetComponent<NetworkIdentity>().netId, element, name);
        }
    }

    public void AssignOnClient(string element, string name)
    {
        if (element == "F")
        {
            SparkerAnimator.CrossFade("menu_pick", 0.01f);
            GetComponent<AudioSource>().PlayOneShot(SparkerAudioClip);
            ElementsDataHelper.singleton.FireName = name;
            FireUsername = ElementsDataHelper.singleton.FireName;
            if (FireUsername == WaterUsername)
            {
                ElementsDataHelper.singleton.WaterName = "";
                WaterUsername = ElementsDataHelper.singleton.WaterName;
            }
            if (ElementsDataHelper.singleton.WaterName == "") AquannaAnimator.CrossFade("menu_idle", 0.01f);

        }
        else if (element == "W")
        {
            AquannaAnimator.CrossFade("menu_pick", 0.01f);
            GetComponent<AudioSource>().PlayOneShot(AquannapickAudioClip);

            ElementsDataHelper.singleton.WaterName = name;
            WaterUsername = ElementsDataHelper.singleton.WaterName;
            if (FireUsername == WaterUsername)
            {
                ElementsDataHelper.singleton.FireName = "";
                FireUsername = ElementsDataHelper.singleton.FireName;
            }
            if (ElementsDataHelper.singleton.FireName == "") SparkerAnimator.CrossFade("idle", 0.01f);

        }

    }
}