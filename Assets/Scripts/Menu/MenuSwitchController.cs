﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuSwitchController : MonoBehaviour {
	
	EventSystem system;

	void Start(){
		system = EventSystem.current;
	}

	public void Update(){
		handleTab ();
		handleEnter ();
	}

	void handleEnter(){
		if (Input.GetKeyDown(KeyCode.Return)){
			if (system.currentSelectedGameObject != null) {
				if(system.currentSelectedGameObject.activeInHierarchy){
					if (system.currentSelectedGameObject.GetComponent<MenuSwitchObject> () != null) {
						CheckForEnter (system.currentSelectedGameObject);
						return;
					}
				}
			}
		}
	}

	void handleTab(){
		if (Input.GetKeyDown(KeyCode.Tab)){
			if (system.currentSelectedGameObject != null) {
				if(system.currentSelectedGameObject.activeInHierarchy){
					if (system.currentSelectedGameObject.GetComponent<MenuSwitchObject> () != null) {
						FindNextElement (system.currentSelectedGameObject);
						return;
					}
				}
			}
			FindFirstElement ();
		}
	}

	void FindFirstElement(){
		MenuSwitchObject[] switchObjects = GameObject.FindObjectsOfType<MenuSwitchObject> ();
		foreach (MenuSwitchObject obj in switchObjects) {
			if (obj.IsFirstObject() && obj.gameObject.activeInHierarchy) {
				ActivateElement (obj.gameObject);
				return;
			}
		}
	}

	void FindNextElement(GameObject obj){
		GameObject objNext = obj.GetComponent<MenuSwitchObject> ().getNextObject ();
		if (objNext != null) {
			ActivateElement (objNext);
		} else {
			FindFirstElement ();
		}
	}

	void CheckForEnter(GameObject obj){
		if (obj.GetComponent<MenuSwitchObject> ().getEnterFunction() != null) {
			obj.GetComponent<MenuSwitchObject> ().getEnterFunction ().Invoke ();
		}
	}



	void ActivateElement(GameObject obj){
		InputField inputfield = obj.GetComponent<InputField>();
		if (inputfield !=null){
			inputfield.OnPointerClick(new PointerEventData(system));
		}
	
		system.SetSelectedGameObject(obj, new BaseEventData(system));
	}

}
