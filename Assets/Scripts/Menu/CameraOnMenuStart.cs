﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CameraOnMenuStart : MonoBehaviour {

	public GameObject sceneCanvas;
	public GameObject blackPanel;
	public HostGame hostGameModule;

	public bool blackOutRunning = false;
	public bool d3Level;

	float timeToLerp = 8f;
	float timeLerped = 0f;
	Quaternion originalRotation;
	int speed = 15;
	delegate void crossDelegate();
	crossDelegate crossPanelMethod;

	//Main : 

	void Start () {
		if (d3Level) {
			//StartCoroutine (ClearOut ());
			StartCoroutine(changeScreen(Color.black, Color.clear, false));
		} else {
			originalRotation = this.transform.rotation;

			crossPanelMethod = turnOffBlackPanel;
			StartCoroutine (changeRotation(new Vector3(3.37f, 9.75f, 0f), 0.2f));

			if (PlayerPrefs.GetInt ("Blackout") == 1) {
				PlayerPrefs.SetInt ("Blackout", 0);
				//StartCoroutine (ClearOut ());
				StartCoroutine(changeScreen(Color.black, Color.clear, false));
			}
		}
	}

	//Public functions used in buttons :

	public void MAIN(){
		crossPanelMethod = FindObjectOfType<MainMenuButtons> ().BackToMain;
		StartCoroutine (changeRotation (new Vector3(0f,9.75f,0f)));
	}

	public void AUTHORS(int x){
		crossPanelMethod = FindObjectOfType<MainMenuButtons> ().AuthorsMenu;
		StartCoroutine (changeRotation (new Vector3(0f,x,0f)));
	}

	public void OPTIONS(int x){
		crossPanelMethod = FindObjectOfType<MainMenuButtons> ().OptionsMenu;
		StartCoroutine (changeRotation (new Vector3(0f,x,0f)));
	}

	public void SIGNOUT(){
		crossPanelMethod = UserAccountManager.instance.LogOut;
		StartCoroutine (changeRotation (originalRotation.eulerAngles, 0.2f, false));
	}

	public void HOSTGAME(){
		if (hostGameModule != null && hostGameModule.roomText.text != "") {
			crossPanelMethod = FindObjectOfType<HostGame> ().CreateRoom;
			StartCoroutine (changeRotation (originalRotation.eulerAngles, 0.2f, false));
		}
	}

	public void BACKTOMAIN(){
		StartCoroutine (ToSkyRotation ("Lobby"));
	}

	//Coroutines

	IEnumerator changeRotation(Vector3 input, float timeToRotate = 0.1f, bool enableTurningsceneCanvasOn = true){
		sceneCanvas.SetActive (false);
		for ( float t = 0f; t < timeToRotate; t+= Time.deltaTime/speed ) {
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(input) , t);
			yield return new WaitForEndOfFrame ();
		}
		if(enableTurningsceneCanvasOn)
			sceneCanvas.SetActive (true);

		crossPanelMethod ();
		yield return null;
	}

	// BlackOut screen, using in LevelManager, PauseMenu and PlayerInteract, clearOut only on Start here.
	public IEnumerator changeScreen(Color fromColor, Color destinationColor, bool blackOut){
		timeLerped = 0f;
		blackPanel.transform.parent.gameObject.SetActive (true);
		if (d3Level && blackOut) 
			sceneCanvas.SetActive (false);
		
		blackOutRunning = true;
		blackPanel.GetComponent<Image> ().color = fromColor;
		if (!blackOut) 
			blackPanel.SetActive (true);
		while (blackPanel.GetComponent<Image> ().color != destinationColor) {
			blackPanel.GetComponent<Image> ().color = Color.Lerp (blackPanel.GetComponent<Image> ().color, destinationColor, timeLerped/timeToLerp);
			timeLerped += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
		if (!blackOut) 
			blackPanel.transform.parent.gameObject.SetActive (false);
		blackOutRunning = false;
	}

	public IEnumerator ToSkyRotation(string name){
		sceneCanvas.SetActive (false);
		for ( float t = 0f; t < 0.2f; t+= Time.deltaTime/speed ) {
			transform.rotation = Quaternion.Lerp(transform.rotation, originalRotation , t);
			yield return new WaitForEndOfFrame ();
		}
		SceneManager.LoadScene(name);
		yield return null;
	}


	//Additional functions

	void turnOffBlackPanel(){
		blackPanel.transform.parent.gameObject.SetActive (false);
	}
}
