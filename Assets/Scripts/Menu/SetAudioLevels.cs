﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetAudioLevels : MonoBehaviour {

	public AudioMixer mainMixer;					//Used to hold a reference to the AudioMixer mainMixer
	public Text graphicsName;

	void Start(){
		graphicsName.text = QualitySettings.names [QualitySettings.GetQualityLevel ()];
	}

	//Call this function and pass in the float parameter musicLvl to set the volume of the AudioMixerGroup Music in mainMixer
	public void SetMusicLevel(float musicLvl){
		mainMixer.SetFloat("MasterVolume", musicLvl);
	}

	public void ChangeGraphics(int i){
		if (i > 0) {
			QualitySettings.IncreaseLevel ();
		} else {
			QualitySettings.DecreaseLevel ();
		}
		graphicsName.text = QualitySettings.names [QualitySettings.GetQualityLevel ()];
	}
}
