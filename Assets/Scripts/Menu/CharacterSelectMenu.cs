﻿using UnityEngine;
using System.Collections;

public class CharacterSelectMenu : MonoBehaviour {
    [SerializeField]
    GameObject pauseMenu;


//	  FIXME : Not used?
//
//    private PlayerController controller;
//
//    public void SetController(PlayerController _controller)
//    {
//        controller = _controller;
//    }
//

    void Start()
    {
        PauseMenu.IsOn = false;
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        } 
    }

    public void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        PauseMenu.IsOn = pauseMenu.activeSelf;
    }
}
