﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelManager : NetworkBehaviour{

	public GameObject levelChoosepanel;
	public Button startGameButton;

	float yChange;
	bool panelShowed = false;


	void Start(){
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = false;
		yChange = levelChoosepanel.GetComponent<RectTransform> ().rect.height - (levelChoosepanel.GetComponent<RectTransform> ().rect.height - Screen.height)/2;
		levelChoosepanel.GetComponent<RectTransform> ().localPosition -= new Vector3 (0f, yChange, 0f);
	}

    [Command]
    public void CmdStartGame(string levelName){
		RpcBlackOut (levelName);
    }

	[ClientRpc]
	public void RpcBlackOut(string levelName){
		//StartCoroutine(FindObjectOfType<CameraOnMenuStart> ().BlackOut ());
		StartCoroutine(FindObjectOfType<CameraOnMenuStart> ().changeScreen(Color.clear, Color.black, true));
		StartCoroutine (CheckIfSwitchScene (FindObjectOfType<CameraOnMenuStart> (), levelName));
	}

	IEnumerator CheckIfSwitchScene(CameraOnMenuStart camera, string levelName){
		while (camera.blackOutRunning) {
			yield return new WaitForEndOfFrame ();
		}
		if (isServer) {
			foreach(PlayerForMenu player in FindObjectsOfType<PlayerForMenu>()){
				player.TurnPlayerFunctions (true);
			}

			NetworkManager.singleton.ServerChangeScene(levelName);
		}
	}

	public void TurnOnChoosingPanel(){
		if (panelShowed == false) {
			panelShowed = true;
			StartCoroutine (ShowChoosingPanel ());
		}
	}

	[Command]
	public void CmdTurnOffMouseIndicator(){
		RpcTurnOffMouseIndicator ();
	}

	[ClientRpc]
	void RpcTurnOffMouseIndicator(){
		MouseFollow.singleton.changeVisible (false);
	}

	IEnumerator ShowChoosingPanel(){
		levelChoosepanel.SetActive (true);
		RectTransform transform = levelChoosepanel.GetComponent<RectTransform> ();
		for (int i = 0; i < yChange/10; i++) {
			transform.anchoredPosition += new Vector2 (0f, 10f);
			yield return new WaitForEndOfFrame ();
		}
		yield return null;
	}
}
