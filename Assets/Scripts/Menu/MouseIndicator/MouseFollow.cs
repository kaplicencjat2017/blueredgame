﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MouseFollow : MonoBehaviour{

	public static MouseFollow singleton;
	public Vector3 position;
	public float speed = Mathf.Infinity;
	public bool visible = true;

	void Awake(){
		if (singleton != null) {
			this.gameObject.SetActive (false);
		} else {
			singleton = this;
			DontDestroyOnLoad (this.gameObject);
			Cursor.visible = false;
		}
	}


	void Update(){
		if (visible) {
			FollowMouse ();
		}
	}


	void FollowMouse(){
		Vector3 mousePosition = Input.mousePosition;
		//mousePosition.z = -Camera.main.transform.localPosition.z; co to kurde było
		mousePosition.z = 3f;
		position = Vector3.Lerp(position, Camera.main.ScreenToWorldPoint(mousePosition), Time.deltaTime * speed);
		transform.position = position;
	}

	public void changeVisible(bool _flag){
		visible = _flag;
		for (int i = 0; i < this.transform.childCount; i++) {
			this.transform.GetChild (i).gameObject.SetActive (_flag);
		}
	}
}