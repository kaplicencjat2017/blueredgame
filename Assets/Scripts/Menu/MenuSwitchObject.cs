﻿using UnityEngine;
using System.Collections;

public class MenuSwitchObject : MonoBehaviour {

	[SerializeField] GameObject nextObject;
	[SerializeField] bool isFirstObject;

	[SerializeField] NoParamAction onEnterClick;

	public GameObject getNextObject(){
		return nextObject;
	}

	public bool IsFirstObject(){
		return isFirstObject;
	}

	public NoParamAction getEnterFunction(){
		return onEnterClick;
	}
}
