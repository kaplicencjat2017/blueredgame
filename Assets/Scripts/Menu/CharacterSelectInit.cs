﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelectInit : NetworkBehaviour{
    
	public GameObject clientPanel;
	public GameObject serverPanel;

	void Start(){
		if (isServer) {
			serverPanel.SetActive (true);
			clientPanel.SetActive (false);
		} else {
			serverPanel.SetActive (false);
			clientPanel.SetActive (true);
		}
	}
}
