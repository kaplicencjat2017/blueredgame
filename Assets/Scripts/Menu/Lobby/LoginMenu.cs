using UnityEngine;
using System;
using System.Collections;
public class LoginMenu : MonoBehaviour {

	public GameObject login_object;
	public GameObject register_object;
	public GameObject loading_object;

	[Header("Input Fields Login")]
	public UnityEngine.UI.InputField input_login_username;
	public UnityEngine.UI.InputField input_login_password;
	
	[Header("Input Fields Register")]
	public UnityEngine.UI.InputField input_register_username;
	public UnityEngine.UI.InputField input_register_password;
	public UnityEngine.UI.InputField input_register_confirmPassword;
	
	[Header("Error Texts")]
	public UnityEngine.UI.Text login_error;
	public UnityEngine.UI.Text register_error;

	// 0 = login, 1 = register, 2 = logged in, 3 = loading
	int part = 0;

	void Start () {
		blankErrors();
	}

	void Update () {
		if (part == 0) {
			login_object.gameObject.SetActive (true);
			register_object.gameObject.SetActive (false);
			loading_object.gameObject.SetActive (false);
		}
		if (part == 1) {
			login_object.gameObject.SetActive (false);
			register_object.gameObject.SetActive (true);
			loading_object.gameObject.SetActive (false);
		}
		if (part == 2) {
			// We are logged in - We have already transitioned to a new scene... Hopefully!
		}
		if (part == 3) {
			login_object.gameObject.SetActive (false);
			register_object.gameObject.SetActive (false);
			loading_object.gameObject.SetActive (true);
		}
		
	}

	void blankErrors () {
		login_error.text = "";
		register_error.text = "";
	}
	
	public void login_Register_Button () {
		part = 1;
		blankErrors();
	}
	
	public void register_Back_Button () {
		part = 0;
		blankErrors();
	}
	
	public void data_LogOut_Button () {
		part = 0;
		UserAccountManager.instance.LogOut();
		blankErrors();
	}

	public void login_login_Button () {
		if ((input_login_username.text != "") && (input_login_password.text != "")) {
			if ((input_login_username.text.Contains ("-")) || (input_login_password.text.Contains ("-"))) {
					login_error.text = "Unsupported Symbol '-'";
					input_login_password.text = "";
			} else {
				StartCoroutine (sendLoginRequest (input_login_username.text, input_login_password.text));
                part = 3;
			}
			
		} else {
			login_error.text = "Field Blank!";
			input_login_password.text = "";
		}
	}

	public void pluszak_login_Button(){
		StartCoroutine(sendLoginRequest("pluszak", "pluszak"));
		part = 3;
	}

    public void adammak_login_Button(){
		StartCoroutine(sendLoginRequest("adammak", "adammak"));
		part = 3;
    }

	public void Quit(){
		Application.Quit ();
	}

    IEnumerator sendLoginRequest (string username, string password) {
		WWWForm form = new WWWForm ();
		form.AddField ("user", username);
		form.AddField ("password", password);

		WWW w = new WWW ("bluered.dx.am/login.php", form);

		yield return w;
		if (w.error == null) {
			if (w.text == "login-SUCCESS") {
				blankErrors ();
				part = 2;
				input_login_username.text = "";
				UserAccountManager.instance.LogIn (username, password);
			} else{
				part = 0;
				login_error.text = w.text;
			}
		} else {
			part = 0;
			login_error.text = "Error " + w.error.ToString ();
		}
	}

	//Register 

	public void register_register_Button () {
			if ((input_register_username.text != "") && (input_register_password.text != "") && (input_register_confirmPassword.text != "")) {
				if (input_register_username.text.Length > 4) {
					if (input_register_password.text.Length > 6) {
						if (input_register_password.text == input_register_confirmPassword.text) {
							if ((input_register_username.text.Contains ("-")) || (input_register_password.text.Contains ("-")) || (input_register_confirmPassword.text.Contains ("-"))) {
								register_error.text = "Unsupported Symbol '-'";
								input_login_password.text = "";
								input_register_confirmPassword.text = "";
							} else {
								StartCoroutine (sendRegisterRequest (input_register_username.text, input_register_password.text));
								part = 3;
							}
						} else {
							register_error.text = "Passwords don't match!";
							input_register_password.text = "";
							input_register_confirmPassword.text = "";
						}
					} else {
						register_error.text = "Password too Short";
						input_register_password.text = "";
						input_register_confirmPassword.text = "";
					}
				} else {
					register_error.text = "Username too Short";
					input_register_password.text = "";
					input_register_confirmPassword.text = "";
				}
			} else {
				register_error.text = "Field Blank!";
				input_register_password.text = "";
				input_register_confirmPassword.text = "";
			}
		
	}
	
	IEnumerator sendRegisterRequest (string username, string password) {

		WWWForm form = new WWWForm ();
		form.AddField ("user", username);
		form.AddField ("password", password);

		WWW w = new WWW ("bluered.dx.am/register.php", form);
		yield return w;

		if (w.error == null) {
			if (w.text == "User has been created!") {
				blankErrors ();
				part = 2;
				input_login_username.text = "";
				UserAccountManager.instance.LogIn (username, password);
			} else {
				part = 0;
				login_error.text = "Error " + w.text.ToString ();
			}
		} else {
			part = 0;
			login_error.text = "Error " + w.error.ToString ();
		}
	}

}
