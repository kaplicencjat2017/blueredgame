using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UserAccountManager : MonoBehaviour {

	public static UserAccountManager instance;

	void Awake ()
	{
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		}

		instance = this;
		DontDestroyOnLoad(this);
	}

	public static string LoggedIn_Username { get; protected set; }

	public static bool IsLoggedIn { get; protected set; }

	public string loggedInSceneName = "Lobby";
	public string loggedOutSceneName = "LoginMenu";

	public void LogOut (){
		LoggedIn_Username = "";

		IsLoggedIn = false;

		SceneManager.LoadScene(loggedOutSceneName);
	}

	public void LogIn(string username, string password){
		LoggedIn_Username = username;
		IsLoggedIn = true;

		StartCoroutine (FindObjectOfType<CameraOnMenuStart> ().ToSkyRotation (loggedInSceneName));

	}

}
