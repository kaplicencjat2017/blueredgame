﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.Collections.Generic;

public class ShowCapsLock : MonoBehaviour {

	[DllImport("USER32.dll")] 
	public static extern short GetKeyState(int nVirtKey);
	public List<Text> texts;

	void Update () {
		if ((GetKeyState(0x14) & 1)>0) //0x14 = VK_CAPITAL (Caps Lock)
			setText("CapsLock ON!");
		else
			setText("");
	}

	void setText(string text){
		for (int i = 0; i < texts.Count; i++) {
			texts [i].text = text;
		}
	}
}
