﻿using UnityEngine;
using System.Collections;

public class DestroyAfterSeconds : MonoBehaviour {

	Vector3 downPos;
	Vector3 upPos;
    public AudioClip PingAudioClip;

	void Start () {
        GetComponent<AudioSource>().PlayOneShot(PingAudioClip);
		downPos = this.transform.position + new Vector3 (0f, 0f, 0f);
		upPos = this.transform.position + new Vector3 (0f, 5f, 0f);
		StartCoroutine (DestroyAfter ());
		StartCoroutine (MoveUpAndDown ());
	}
	
	IEnumerator DestroyAfter(){
		yield return new WaitForSeconds(3f);
		Destroy(this.gameObject);
	}

	IEnumerator MoveUpAndDown(){
		while (true) {
			transform.position = Vector3.Lerp(upPos, downPos,
				Mathf.SmoothStep(0f,1f, Mathf.PingPong(Time.time, 1f)));
			yield return new WaitForEndOfFrame ();
		}
	}
}
