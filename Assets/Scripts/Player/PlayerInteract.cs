﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerInteract : NetworkBehaviour {

	[SerializeField]Transform pickPos;

	GameObject pickedObj;

	Camera myCamera;

	InteractableObject thingToInteract = null;

    public AudioClip pickAudioClip;

	float distance = 2;

	bool isPickingObject = false;
	bool canInteract = false;

	Vector3 prevPositionOfObject = Vector3.zero;


	#region Setters Getters : 

	public void setPickedObj(GameObject _object){
		pickedObj = _object;
	}

	public void setPickingObject(bool _flag){
		isPickingObject = _flag;
	}

	#endregion

	void Start(){
		myCamera = GetComponent<PlayerMotor> ().cam;
	}

	void Update () {
		if (!isLocalPlayer)
			return;

		handlePickPosition ();

		CmdHandlePickedObj ();
		LeftMouseButtonUse ();
	}

	void handlePickPosition(){
		if (PauseMenu.IsOn) {
			return;
		}
		Vector3 mockPospickPos = pickPos.localPosition;
		mockPospickPos = new Vector3 (0f, Input.mousePosition.y, distance);
		mockPospickPos = myCamera.ScreenToWorldPoint (mockPospickPos);
		pickPos.position = mockPospickPos;
		pickPos.localPosition = new Vector3 (0f, pickPos.localPosition.y, pickPos.localPosition.z);
		if (pickPos.localPosition.y > -1.5f) {
			prevPositionOfObject = pickPos.localPosition;
		} else {
			pickPos.localPosition = prevPositionOfObject;
		}

	}

	void LeftMouseButtonUse(){
		if (!PauseMenu.IsOn) {
			if (Input.GetMouseButtonDown (0)) {
				if (pickedObj != null) {
					CmdDropObject ();
				} else{
					CheckRaycast ();
				}
			}
		}
	} 

	void CheckRaycast(){
		RaycastHit hit = new RaycastHit ();
		Ray ray = myCamera.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit, 2f)) {
			if (hit.collider.transform.tag == "PickAble") {
				CmdPickObject (hit.collider.gameObject);
				return;
			} 
		}
		CheckForInteract ();
	}
	void CheckForInteract(){
		if (canInteract && thingToInteract != null) {
			RaycastHit hit = new RaycastHit ();
			Ray ray = myCamera.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit, 4f)) {
				if (hit.collider.gameObject == thingToInteract.gameObject) {
					CmdInteract ();
					return;
				} else {
					Debug.Log (hit.collider.gameObject.name + " but interactable is " + thingToInteract.gameObject.name);
				}
			}

		} 
	}

	#region Pickable Objects :

	[Command]
	void CmdPickObject(GameObject hit){
		if (!hit.GetComponent<PickAbleObject> ().IsPicked() && !isPickingObject) {
			isPickingObject = true;
			RpcPickObject (hit);
		}
	}

	[ClientRpc]
	void RpcPickObject(GameObject hit){
        GetComponent<AudioSource>().PlayOneShot(pickAudioClip);
		hit.GetComponent<PickAbleObject> ().Pick (this.transform, pickPos.localPosition, this);
	}

	[Command]
	void CmdHandlePickedObj(){
		RpcHandlePickedObject ();
	}

	[ClientRpc]
	void RpcHandlePickedObject(){
		if (pickedObj != null) {
			pickedObj.transform.position = pickPos.position;
			pickedObj.transform.localRotation = Quaternion.Euler (0f, 0f, 0f);
		}
	}

	[Command]
	void CmdDropObject(){
		pickedObj.GetComponent<PickAbleObject> ().LeftObject ();
		RpcDropObject (pickedObj);
	}

	[Command]
	public void CmdDropObjectByDistance(GameObject object_bot_picked_yet){
		isPickingObject = false;
		RpcDropObject (object_bot_picked_yet);
	}

	[ClientRpc]
	void RpcDropObject(GameObject _picked_object){
		_picked_object.transform.SetParent (null);
		_picked_object.GetComponent<Rigidbody> ().useGravity = true;
		_picked_object.GetComponent<Rigidbody> ().isKinematic = false;
		_picked_object.GetComponent<BoxCollider> ().enabled = true;
		pickedObj = null;
	}

	#endregion

	#region Interactable objects

	public void SetVariables(Lever _interactable, bool _canInteract){
		thingToInteract = _interactable;
		canInteract = _canInteract;
	}

	[Command]
	public void CmdInteract(){
		if (thingToInteract.canUse()) {
			thingToInteract.ChangeState ();
		}
	}

	#endregion

	#region Ending trigger in/out

	public void StepIntoEnd(int max){
		if (isLocalPlayer) {
			CmdStepIntoEnd (max);
		} else {
			Console.Log ("Not this player!");
		}
	}

	[Command]
	private void CmdStepIntoEnd(int max){
		GameManager.instance.PlayersInEnd++;
		if (GameManager.instance.PlayersInEnd >= max) {
			RpcShowWhatToDoNextPanel ();
		}
	}

	public void StepOutOfEnd(){
		if (isLocalPlayer) {
			CmdStepOutOfEnd ();
		}
	}

	[Command]
	private void CmdStepOutOfEnd(){
		GameManager.instance.PlayersInEnd--;
	}

	[ClientRpc]
	void RpcShowWhatToDoNextPanel(){
		foreach (PlayerController player in FindObjectsOfType<PlayerController>()) {
			player.enabled = false;
			player.GetComponent<PlayerMotor> ().enabled = false;
		}

		PauseMenu.IsOn = true;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;


		if (isServer) {
			TurnOnMenu (0);
		} else {
			TurnOnMenu (1);
		}
	}

	void TurnOnMenu(int i){
		GameManager.instance.WhatToDoAfterLevelPanel.transform.GetChild (i).gameObject.SetActive (true);
	}

	#endregion

	#region ConsoleLogs

	public void ConsoleOnAll(string message){
		if (isLocalPlayer) {
			CmdLog (this.GetComponent<Player> ().username, message);
		}
	}

	[Command]
	private void CmdLog(string username, string message){
		RpcLog (username, message);
	}

	[ClientRpc]
	private void RpcLog(string username, string message){
		Console.Log (username + message);
	}

	#endregion

	#region Pings

	public void PingOnAll(Vector3 pos){
		if (isLocalPlayer) {
			CmdPing (pos, Color.clear);
		}
	}

	public void PingOnAll(Vector3 pos, Color color){
		if (isLocalPlayer) {
			CmdPing (pos, color);
		}
	}

	[Command]
	private void CmdPing(Vector3 pos, Color color){
		RpcPing (pos, color);
	}

	[ClientRpc]
	private void RpcPing(Vector3 pos, Color color){
		if (GameManager.instance.pingArrow == null) {
			Debug.Log ("Przypisz strzałkę w gameManager");
			return;
		}

		GameObject arrow = Instantiate(GameManager.instance.pingArrow, pos + new Vector3(0f, 4f, 0f), Quaternion.Euler(0f,0f,90f)) as GameObject;
		if (color != Color.clear) {
			arrow.GetComponent<Renderer> ().material.color = color;
		}
	}

	#endregion


	#region Going to next level with blackOut

	[Command]
	public void CmdStartGame(string levelName){
		RpcBlackOut (levelName);
	}

	[ClientRpc]
	public void RpcBlackOut(string levelName){
		//StartCoroutine(FindObjectOfType<CameraOnMenuStart> ().BlackOut ());
		StartCoroutine(FindObjectOfType<CameraOnMenuStart> ().changeScreen(Color.clear, Color.black, true));
		StartCoroutine (CheckIfSwitchScene (FindObjectOfType<CameraOnMenuStart> (), levelName));
	}

	IEnumerator CheckIfSwitchScene(CameraOnMenuStart camera, string levelName){
		while (camera.blackOutRunning) {
			yield return new WaitForEndOfFrame ();
		}
		if (isServer) {
			foreach(PlayerForMenu player in FindObjectsOfType<PlayerForMenu>()){
				player.TurnPlayerFunctions (true);
			}

			NetworkManager.singleton.ServerChangeScene(levelName);
		}
	}

	#endregion
}
