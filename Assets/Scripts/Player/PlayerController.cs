using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{

    #region Variables + get/set 

    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;

    [SerializeField]
    public float thrusterForce = 10f;
    float groundDistance;
	float width;

    private PlayerMotor motor;
	CapsuleCollider capsuleCollider;
    public bool isLanding = false;

	bool jumping = false;

    #endregion

    void Start()
    {
        motor = GetComponent<PlayerMotor>();
		capsuleCollider = this.GetComponent<CapsuleCollider> ();
		groundDistance = capsuleCollider.height;
		width = capsuleCollider.radius/2;
    }

    void Update(){
        if (PauseMenu.IsOn){
            if (Cursor.lockState != CursorLockMode.None)
                Cursor.lockState = CursorLockMode.None;

            motor.Move(Vector3.zero);
            motor.Rotate(Vector3.zero);
            motor.RotateCamera(0f);
            return;
        }

        if (Cursor.lockState != CursorLockMode.Locked){
            Cursor.lockState = CursorLockMode.Locked;
        }

        MovePlayer();
        RotatePlayer();
        jump();

      
    }

    private void MovePlayer(){
        float _xMov = Input.GetAxis("Horizontal");
        float _zMov = Input.GetAxis("Vertical");

        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVertical = transform.forward * _zMov;

        Vector3 _velocity = (_movHorizontal + _movVertical) * speed;

        motor.Move(_velocity);
    }

    private void RotatePlayer()
    {
        //Calculate rotation as a 3D vector (turning around)
        float _yRot = Input.GetAxisRaw("Mouse X");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;

        //Apply rotation
        motor.Rotate(_rotation);

        //Calculate camera rotation as a 3D vector (turning around)
        float _xRot = Input.GetAxisRaw("Mouse Y");

        float _cameraRotationX = _xRot * lookSensitivity;

        //Apply camera rotation
        motor.RotateCamera(_cameraRotationX);
    }

    void jump()
    {
        Vector3 _thrusterForce = Vector3.zero;

  
        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            motor.audioSource.PlayOneShot(motor.JumpAudioClip);
            _thrusterForce = Vector3.up * thrusterForce;
            StartCoroutine(JumpSequencEnumerator());
        }


        motor.ApplyThruster(_thrusterForce);
    }

    public IEnumerator JumpSequencEnumerator()
    {
        yield return new WaitForSeconds(0.5f);
        isLanding = true;
    }

    public bool isGrounded()
    {
		if (jumping) {
			return false;
		} else {
			StartCoroutine (DontDoubleJump ());
			//return CheckSeveralPointsRaycast ();\
			return Physics.Raycast(transform.position, -Vector3.up, groundDistance + 0.1f);
		}
    }

	IEnumerator DontDoubleJump(){
		jumping = true;
		yield return new WaitForSeconds (0.1f);
		jumping = false;
	}

	bool CheckSeveralPointsRaycast(){
		int output = 0;

		Vector3[] startingPositionsForRaycast = new Vector3[8]{ 
			transform.position + new Vector3(0.1f, 0f, 0f), 
			transform.position + new Vector3(-0.1f, 0f, 0f), 
			transform.position + new Vector3(0f, 0f, 0.1f), 
			transform.position + new Vector3(0f, 0f, -0.1f), 
			transform.position + new Vector3(width, 0f, width), 
			transform.position + new Vector3(-width, 0f, width),
			transform.position + new Vector3(width, 0f, -width),
			transform.position + new Vector3(-width, 0f, -width)};

		foreach (Vector3 point in startingPositionsForRaycast) {
			if (Physics.Raycast (point, -Vector3.up, groundDistance + 0.01f)) {
				output++;
				if (output > 4) {
					break;
				}
			}
		}

		if (output > 4) {
			return true;
		} else {
			return false;
		}
	}
}
