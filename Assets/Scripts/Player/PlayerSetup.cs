using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(PlayerController))]
public class PlayerSetup : NetworkBehaviour{
    
	[SerializeField] Behaviour[] componentsToDisable;

    [SerializeField] string remoteLayerName = "RemotePlayer";

    [SerializeField] string dontDrawLayerName = "DontDraw";
    [SerializeField] GameObject playerGraphics;

    [SerializeField] GameObject playerUIPrefab;
    [HideInInspector] public GameObject playerUIInstance;

	[SerializeField] private Text usernametext;

	[SerializeField] private Text elementtext;

 
    [SerializeField] private AudioClip spawnAudioClip;

    void Start()
    {
        if (!isLocalPlayer)
        {
            DisableComponents();
            AssignRemoteLayer();
			assignPlayerElements ();
        }
        else
        {
            // Disable player graphics for local player
            SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));

            // Create PlayerUI
            playerUIInstance = Instantiate(playerUIPrefab);
            playerUIInstance.name = playerUIPrefab.name;

            // Configure PlayerUI
            PlayerUI ui = playerUIInstance.GetComponent<PlayerUI>();
			if (ui == null) {
				Console.Log ("No PlayerUI component on PlayerUI prefab.");
			}

            GetComponent<Player>().SetupPlayer();

            string _username = "Loading...";
            if (UserAccountManager.IsLoggedIn)
            {
                _username = UserAccountManager.LoggedIn_Username;
            }
            else
            {
                _username = transform.name;
            }

			CmdSetUsername(transform.name, _username);

        }
    }

	void turnOnMyCanvas(){
		if (ElementChooseOnLevel.singleton == null)
			return;

		if (this.GetComponent<Player> ().PlayerElement.ToString () == "FIRE") {
			ElementChooseOnLevel.singleton.FireElementPanel.SetActive (true);
			ElementChooseOnLevel.singleton.WaterElementPanel.SetActive (false);
		} else if (this.GetComponent<Player> ().PlayerElement.ToString () == "WATER") {
			ElementChooseOnLevel.singleton.WaterElementPanel.SetActive (true);
			ElementChooseOnLevel.singleton.FireElementPanel.SetActive (false);
		} else {
			Debug.Log ("Coś się zromblowało : " + this.GetComponent<Player> ().PlayerElement.ToString ());
		}
	}

	void assignPlayerElements()
	{
	      StartCoroutine(playSpawnSound());
        if (GetComponent<Player> ().username == ElementsDataHelper.singleton.FireName) {
			GetComponent<Player> ().PlayerElement = PLAYERELEMENT.FIRE;
			GetComponent<Player> ().GetComponent<ChangeColorOfPlayer> ().MakeRed ();
		} else if (GetComponent<Player> ().username == ElementsDataHelper.singleton.WaterName) {
			GetComponent<Player> ().PlayerElement = PLAYERELEMENT.WATER;
			GetComponent<Player> ().GetComponent<ChangeColorOfPlayer> ().MakeBlue ();
		} else {
			Debug.LogWarning (GetComponent<Player> ().username);
		}
		handlePlayerTemplate ();
		if (isLocalPlayer) {
			turnOnMyCanvas ();
		}
	}

	void handlePlayerTemplate(){
		Player player = GetComponent<Player> ();
		usernametext.text = player.username;
		elementtext.text = player.PlayerElement.ToString();
	}

    [Command]
    void CmdSetUsername(string playerID, string username){
		RpcSetUsername (playerID, username);
    }

	[ClientRpc]
	void RpcSetUsername(string playerID, string username){
		//Player player = GameManager.GetPlayer(playerID);
		//if (player != null) {
			Console.Log (username + " has joined!");
			GetComponent<Player>().username = username;
			assignPlayerElements ();
		//}
	}


    [ClientRpc]
    void RpcAnnounceJoining(string username)
    {
        Console.Log(username + " has joined!");
    }

    void SetLayerRecursively(GameObject obj, int newLayer)
    {
        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        //string _netID = GetComponent<NetworkIdentity>().netId.ToString();
        //Player _player = GetComponent<Player>();

        //GameManager.RegisterPlayer(_netID, _player);
    }

    void AssignRemoteLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
    }

    void DisableComponents()
    {
        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsToDisable[i].enabled = false;
        }
    }

    // When we are destroyed
    void OnDisable(){
		if(SceneManager.GetActiveScene().name != "CharacterSelect"){
        	Destroy(playerUIInstance);

			if (isLocalPlayer) {
				GameManager.instance.SetSceneCameraActive (true);
			}
            //GameManager.UnRegisterPlayer(transform.name);
    	}
	}

    void Update()
    {
		if (Input.GetKeyDown("p") && isLocalPlayer)
        {
            foreach (GameObject player in GetAllPlayers())
            {
                Console.Log(player.GetComponent<Player>().username + " " + player.GetComponent<Player>().PlayerElement.ToString());
            }
        }
    }

    public GameObject[] GetAllPlayers()
    {
        return GameObject.FindGameObjectsWithTag("Player");
    }
    public IEnumerator playSpawnSound()
    {
        float tempVol = GetComponent<AudioSource>().volume;
        GetComponent<AudioSource>().volume = 0.01f;
        GetComponent<AudioSource>().PlayOneShot(spawnAudioClip);
        yield return new WaitForSeconds(3f);
        GetComponent<AudioSource>().volume = tempVol;

    }

}
