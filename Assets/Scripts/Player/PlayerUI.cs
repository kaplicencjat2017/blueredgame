using UnityEngine;

public class PlayerUI : MonoBehaviour {

	[SerializeField]
	RectTransform thrusterFuelFill;

	[SerializeField]
	GameObject pauseMenu;

	[SerializeField]
	GameObject scoreboard;

	public bool canUse = true;

	void Start ()
	{
		PauseMenu.IsOn = false;
	}

	void Update ()
	{
		if (canUse) {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				TogglePauseMenu ();
			}

			if (Input.GetKeyDown (KeyCode.Tab)) {
				scoreboard.SetActive (true);
			} else if (Input.GetKeyUp (KeyCode.Tab)) {
				scoreboard.SetActive (false);
			}
		}
	}

	public void TogglePauseMenu ()
	{
		pauseMenu.SetActive(!pauseMenu.activeSelf);
		PauseMenu.IsOn = pauseMenu.activeSelf;
		Cursor.visible = PauseMenu.IsOn;
    }

	public void ChangeForEndOfLevel ()
	{
		Cursor.visible = false;
		for (int i = 0; i < pauseMenu.transform.childCount; i++) {
			pauseMenu.transform.GetChild (i).gameObject.SetActive (false);
		}
		canUse = false;
	}

	void SetFuelAmount (float _amount)
	{
		thrusterFuelFill.localScale = new Vector3(1f, _amount, 1f);
	}

}
