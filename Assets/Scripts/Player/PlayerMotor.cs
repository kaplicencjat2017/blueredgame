using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    public Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;
    private Vector3 thrusterForce = Vector3.zero;
   
    [SerializeField]
    private float cameraRotationLimit = 85f;

    Rigidbody rb;
	PlayerInteract interact;

    private Animator aquannaAnimator;
    private Animator sparkerAnimator;

    public GameObject AquannaGameObject;
    public GameObject SparkerGameObject;

    public List<AudioClip> FootstepsAudioClips = new List<AudioClip>();
    private PlayerController playerController;
    public AudioClip JumpAudioClip;
    public AudioClip LandedAudioClip;

    public bool isMoving = false;
    public AudioSource audioSource;

	public float handPointMovingPower;

    void Start()
    {
        playerController = GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody>();
		interact = GetComponent<PlayerInteract> ();
		if (AquannaGameObject != null) {
			aquannaAnimator = AquannaGameObject.GetComponent<Animator> ();
		}

		if (SparkerGameObject != null) {
			sparkerAnimator = SparkerGameObject.GetComponent<Animator> ();
		}
        audioSource = GetComponent<AudioSource>();
    }

    // Gets a movement vector
    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    // Gets a rotational vector
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    // Gets a rotational vector for the camera
    public void RotateCamera(float _cameraRotationX)
    {
        cameraRotationX = _cameraRotationX;
    }

    // Get a force vector for our thrusters
    public void ApplyThruster(Vector3 _thrusterForce)
    {
		thrusterForce = _thrusterForce;
    }



    // Run every physics iteration
    void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();

        if (playerController.isLanding && playerController.isGrounded())
        {
            aquannaAnimator.CrossFade("jump_land", 0.01f);
			if (sparkerAnimator != null) {
				sparkerAnimator.CrossFade ("jump_land", 0.01f);
			}
            audioSource.PlayOneShot(LandedAudioClip);
            playerController.isLanding = false;
        }
    }

    //Perform movement based on velocity variable
    void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            if(!audioSource.isPlaying && playerController.isGrounded())
            audioSource.PlayOneShot(FootstepsAudioClips[Random.Range(0, FootstepsAudioClips.Count - 1)]);

            if (!aquannaAnimator.GetCurrentAnimatorStateInfo(0).IsName("run1") || !sparkerAnimator.GetCurrentAnimatorStateInfo(0).IsName("run1"))
            {
                aquannaAnimator.CrossFade("run1", 0.01f);
				if (sparkerAnimator != null) {
					sparkerAnimator.CrossFade ("run1", 0.01f);
				}
                isMoving = true;
            }
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
            aquannaAnimator.SetBool("isMoving", true);
        }

        if (velocity == Vector3.zero)
        {
           
            if (isMoving)
            {
             
                aquannaAnimator.CrossFade("idle", 0.1f);
				if (sparkerAnimator != null) {
					sparkerAnimator.CrossFade ("idle", 0.1f);
				}
                isMoving = false;
            }
        }

        if (thrusterForce != Vector3.zero){
            aquannaAnimator.CrossFade("jump2", 0.001f);
			if (sparkerAnimator != null) {
				sparkerAnimator.CrossFade ("jump2", 0.001f);
			}
			rb.AddForce(thrusterForce * Time.deltaTime, ForceMode.Impulse);
			thrusterForce = Vector3.zero;


        }

    }

    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null){
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }

}
