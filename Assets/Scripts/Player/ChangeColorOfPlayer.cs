﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ChangeColorOfPlayer : MonoBehaviour {

	public GameObject fireModel;
	public GameObject waterModel;

    public GameObject AquannaGameObject;
    public GameObject SparkerGameObject;

    public GameObject Player;

    public void MakeRed()
    {
		fireModel.SetActive (true);
        Player.GetComponent<NetworkAnimator>().animator = SparkerGameObject.GetComponent<Animator>();
		waterModel.SetActive (false);
    }

    public void MakeBlue()
    {
        Player.GetComponent<NetworkAnimator>().animator = AquannaGameObject.GetComponent<Animator>();
        fireModel.SetActive (false);
		waterModel.SetActive (true);
    }

}
