﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class PlayerForMenu : NetworkBehaviour {

	public Behaviour[] componentsToDisableOnChampionSelect;

	void Awake () {
		TurnPlayerFunctions (false);
	}

	public void TurnPlayerFunctions(bool _flag){
		if (SceneManager.GetActiveScene ().name == "CharacterSelect") {
			foreach (Behaviour beh in componentsToDisableOnChampionSelect) {
				beh.enabled = _flag;
			}

			for (int i = 0; i < this.transform.childCount; i++) {
				transform.GetChild (i).gameObject.SetActive (_flag);
			}
		}
	}

	#region Cubes rotation in menu + changing name

	public void tryAssign(NetworkInstanceId netId, string element, string name){
		if (isLocalPlayer) {
			CmdAssing (netId, element, name);
		}
	}

	[Command]
	void CmdAssing(NetworkInstanceId netId, string element, string name){
		RpcAssing (netId, element, name);
	}

	[ClientRpc]
	void RpcAssing(NetworkInstanceId netId, string element, string name){
		ClientScene.FindLocalObject(netId).GetComponent<CharacterSelectManager>().AssignOnClient (element, name);
	}

	#endregion

}
