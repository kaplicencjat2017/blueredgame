﻿using UnityEngine;
using System.Collections;

public class SwampTrigger : Trigger {

	public override void Awake(){
		goes_alive = PLAYERELEMENT.NONE;
		alias = "swamp";
	}
}
