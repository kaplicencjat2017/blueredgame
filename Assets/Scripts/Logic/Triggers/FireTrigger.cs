﻿using UnityEngine;
using System.Collections;

public class FireTrigger : Trigger {

	public override void Awake(){
		goes_alive = PLAYERELEMENT.FIRE;
		alias = "fire";
	}
}
