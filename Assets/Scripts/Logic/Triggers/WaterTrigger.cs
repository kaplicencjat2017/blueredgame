﻿using UnityEngine;
using System.Collections;

public class WaterTrigger : Trigger {
	public override void Awake(){
		goes_alive = PLAYERELEMENT.WATER;
		alias = "water";
	}
}
