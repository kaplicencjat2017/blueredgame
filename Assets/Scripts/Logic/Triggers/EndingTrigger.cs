﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EndingTrigger : NetworkBehaviour, ITrigger {
	public int playersRequired;

	public void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			other.GetComponent<PlayerInteract> ().StepIntoEnd (playersRequired);
		}
	}

	public void OnTriggerStay(Collider other){

	}

	public void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			other.GetComponent<PlayerInteract> ().StepOutOfEnd ();
		}
	}
}
