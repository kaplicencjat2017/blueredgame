﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour, ITrigger {

	public PLAYERELEMENT goes_alive;
	public string alias = "none";

	public virtual void Awake(){
		goes_alive = PLAYERELEMENT.NONE;
	}
	public void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			Console.Log (other.GetComponent<Player>().username + " steps into " + alias + "!");
			if (CheckIfKill (other.GetComponent<Player> ().PlayerElement)) {
				foreach (Player player in FindObjectsOfType<Player>()) {
					player.isDead = true;
				}
			}
		}
	}
	public void OnTriggerStay(Collider other){

	}
	public void OnTriggerExit(Collider other){

	}

	protected bool CheckIfKill(PLAYERELEMENT element){
		if (goes_alive == PLAYERELEMENT.NONE) {
			return true;
		} else if (goes_alive == PLAYERELEMENT.FIRE) {
			if (element == PLAYERELEMENT.FIRE) {
				return false;
			} else {
				return true;
			}
		}else if (goes_alive == PLAYERELEMENT.WATER) {
			if (element == PLAYERELEMENT.WATER) {
				return false;
			} else {
				return true;
			}
		}
		return true;
	}
}
