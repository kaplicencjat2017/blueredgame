﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Console : NetworkBehaviour {
	#region Variables

	private static Console instance = null;

	public static Console Instance{
		get{
			if (instance == null) {
				instance = FindObjectOfType (typeof(Console)) as Console;
			}
			return instance;
		}
	}

	public GameObject ConsolePanel;
	private Text[] consoleTexts;

	private const int max_messages = 4;
	private int number_of_messages = 0;

	#endregion

	void Awake(){
		instance = this;
		consoleTexts = new Text[max_messages];
		for (int i = 0; i < max_messages; i++) {
			consoleTexts [i] = ConsolePanel.transform.GetChild (i).GetComponent<Text> ();
		}
	}

	#region Log writing

	public static void Log(string _message){
		if (Console.Instance != null) {
			Console.Instance.Write (_message);
		}
	}

	private void Write(string _message){
		ConsolePanel.SetActive (true);
		Add (_message);
		StartCoroutine (WaitForSeconds (3));
	}

	private void Add(string _message){
		if (number_of_messages < max_messages) {
			consoleTexts [number_of_messages].text = _message;
			number_of_messages++;
		} else {
			for (int i = 0; i < max_messages-1; i++) {
				consoleTexts [i].text = consoleTexts [i+1].text;
			}
			consoleTexts [max_messages-1].text = _message;
		}
	}

	IEnumerator WaitForSeconds(int i){
		yield return new WaitForSeconds (i);
		ConsolePanel.SetActive (false);
	}
	#endregion


}
