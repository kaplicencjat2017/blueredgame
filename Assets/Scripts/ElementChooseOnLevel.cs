﻿using UnityEngine;
using System.Collections;

public class ElementChooseOnLevel : MonoBehaviour {

	public static ElementChooseOnLevel singleton;

	void Awake(){
		singleton = this;
	}

	public GameObject FireElementPanel;
	public GameObject WaterElementPanel;
}
