﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class SmallDoors : redblueObject, IDoors {

	[Header("Small Doors")]
	[SerializeField] int players_required;

	int player_avaliable = 0;
	float time_opened = 5f;
	Animation my_Anim;
	bool opened = false;

	string animationName = "smallgateopen";

	void Start(){
		my_Anim = GetComponent<Animation> ();
	}

	public void Doors_try_open(){
		if (!opened) {
			openDoors ();
			StartCoroutine (countdownForClose ());
		}
	}

	public void Doors_try_open_PRESSUREPLATE(){
		player_avaliable++;
		if (!opened && players_required <= player_avaliable) {
			openDoors ();
		}
	}

	public void Doors_tryClose_PRESSUREPLATE(){
		player_avaliable--;
		if (opened && players_required > player_avaliable) {
			CloseDoorsOnServer ();
		}
	}

	public void CloseDoorsOnServer(){
		if (isServer) {
			RpcCloseDoors ();
		}
	}

	IEnumerator countdownForClose(){
		yield return new WaitForSeconds (time_opened);
		CloseDoorsOnServer ();
		yield return null;
	}

	[ClientRpc]
	void RpcCloseDoors(){
		closeDoors ();
	}

	public void openDoors(){
        GetComponent<AudioSource>().Play();

        opened = true;
		if (my_Anim.isPlaying) {
		} else {
			my_Anim [animationName].time = 0f;
		}
		my_Anim [animationName].speed = 1f;
		my_Anim.Play (animationName);
	}

	public void closeDoors(){
        GetComponent<AudioSource>().Play();

        opened = false;
		if (my_Anim.isPlaying) {
		} else {
			my_Anim [animationName].time = my_Anim [animationName].length;
		}
		my_Anim [animationName].speed = -1f;
		my_Anim.Play (animationName);
	}
}
