﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class MovingObject : redblueObject {
	[Header("Moving Object")]
	[SerializeField] float y_change_expect;
	[SerializeField] int playersRequired;
	int playersAvaliable = 0;
	Vector3 originalPos;
	Vector3 endPos;
	int number_of_steps = 80;
	bool turned = true;

	[SyncVar] public bool moving = false; // not sure if need to be public!

	void Start(){
		originalPos = this.transform.position;
		endPos = this.transform.position + Vector3.up * y_change_expect;
	}


	//Lever : 

	public void FunctionForLeverChangeState(){
		if (!moving) {
			if (turned) {
				StartCoroutine(MakeMove (1));
			} else {
				StartCoroutine(MakeMove (-1));
			}
		}
	}

	IEnumerator MakeMove(int _side){
		float step = y_change_expect / number_of_steps;
		moving = true;

		//Up
		if (_side > 0) {
			for (int i = 0; i < number_of_steps; i++) {
				this.transform.position += Vector3.up * step;
				yield return new WaitForEndOfFrame ();
			}
			turned = false;
		}
		//Down
		else {
			for (int i = 0; i < number_of_steps; i++) {
				this.transform.position -= Vector3.up * step;
				yield return new WaitForEndOfFrame ();
			}
			turned = true;
		}

		moving = false;
		yield return null;
	}



	//Pressure Plate : 

	public void PressurePlateMoveUpFunction(){
		StopAllCoroutines ();
		StartCoroutine (MoveUp ());
	}

	IEnumerator MoveUp(){
        GetComponent<AudioSource>().Play();
        while (this.transform.position != endPos) {
			this.transform.position = Vector3.MoveTowards (this.transform.position, endPos, Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
        GetComponent<AudioSource>().Stop();
	}

	public void PressurePlateMoveDownFunction(){
		StopAllCoroutines ();
		StartCoroutine (MoveDown ());
	}

	IEnumerator MoveDown(){
        GetComponent<AudioSource>().Play();
		while (this.transform.position != originalPos) {
			this.transform.position = Vector3.MoveTowards (this.transform.position, originalPos, Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
        GetComponent<AudioSource>().Stop();
    }

	public void PressurePlateAddCounterOfPressurePlates(){
		playersAvaliable++;
		if (playersAvaliable >= playersRequired) {
			PressurePlateMoveUpFunction ();
		}
	}

	public void PressurePlateDecreaseCounterOfPressurePlates(){
		playersAvaliable--;
		if (playersAvaliable < playersRequired) {
			PressurePlateMoveDownFunction ();
		}
	}
}
