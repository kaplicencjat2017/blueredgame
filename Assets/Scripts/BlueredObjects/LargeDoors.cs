﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class LargeDoors : redblueObject, IDoors {

	[Header("Large Doors")]
	Animation my_Anim;
	int player_avaliable = 0;
	bool opened = false;

	[SerializeField] int players_required;

	void Start(){
		my_Anim = GetComponent<Animation> ();
	}

	public void Doors_try_open(){
		player_avaliable++;
		if (player_avaliable >= players_required && !opened) {
			if (!my_Anim.isPlaying) {
				opened = true;
				my_Anim.Play ("GateOpen");
				GetComponent<AudioSource> ().Play ();
			}
		}
	}

	public void Doors_try_close(){
		if (!opened) {
			player_avaliable--;
		}
	}
}
