﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PickAbleObject : redblueObject {

	Rigidbody myRigidbody;
	BoxCollider myCollider;
	bool picked = false;

	Vector3 startingPosition;

	float minDistanceToPick = 0.1f;
	float maxDistanceToPick = 5f;
	float objectSpeed = 3f;
	float objectRotationSpeed = 5f;

	float minMapY = -100f;
	float maxMapY = 100f;

	public bool IsPicked(){
		return picked;
	}

	public void LeftObject(){
		picked = false;
	}

	public BoxCollider getCollider(){
		return myCollider;
	}

	void Start(){
		myRigidbody = this.GetComponent<Rigidbody> ();
		myCollider = GetComponent<BoxCollider> ();
		startingPosition = this.transform.position;
		StartCoroutine (checkIfOnMap ());
	}

	public void Pick(Transform parent, Vector3 pos, PlayerInteract interactPlayer){
		picked = true;
		transform.SetParent (parent);
		myRigidbody.useGravity = false;
		myRigidbody.isKinematic = true;
		StartCoroutine (moveToPosition (pos, interactPlayer));
	}

	IEnumerator moveToPosition(Vector3 pos, PlayerInteract player){
		bool shouldPick = true;

		while (Vector3.Distance(transform.localPosition, pos) > minDistanceToPick && shouldPick) {
			if(Vector3.Distance(transform.localPosition, pos) > maxDistanceToPick){
				objectLeftByDistance (player, out shouldPick);
				yield return new WaitForEndOfFrame ();
			}
			moveObjectToDestination (pos);
			yield return new WaitForEndOfFrame ();
		}

		if (shouldPick) {
			player.setPickedObj(this.gameObject);
			player.setPickingObject(false);
		}
	}

	void objectLeftByDistance(PlayerInteract player, out bool shouldPick){
		player.CmdDropObjectByDistance (this.gameObject);
		LeftObject ();
		shouldPick = false;
	}

	void moveObjectToDestination(Vector3 pos){
		transform.localPosition = Vector3.MoveTowards (transform.localPosition, pos, Time.deltaTime * objectSpeed);
		transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.Euler (0f, 0f, 0f), Time.deltaTime * objectRotationSpeed);
	}

	IEnumerator checkIfOnMap(){
		while (true) {
			if (this.transform.position.y > maxMapY || this.transform.position.y < minMapY) {
				if (isServer) {
					RpcRespawn ();
				}
			}
			yield return new WaitForSeconds (5f);
		}
	}

	[ClientRpc]
	void RpcRespawn(){
		this.transform.position = startingPosition;
		myRigidbody.velocity = Vector3.zero;
	}
}
