﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Lever : InteractableObject {

	[Header("Lever")]
	[SyncVar] bool turned = true;

	Material originalMaterial;
	Animation myAnim;

	[SerializeField] Material usedMaterial;
	[SerializeField] NoParamAction action;

	float timeToWait = 5f;


	void Start(){
		originalMaterial = this.GetComponent<Renderer> ().material;
		myAnim = transform.parent.parent.GetComponent<Animation> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			other.GetComponent<PlayerInteract> ().SetVariables (this, true);
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			other.GetComponent<PlayerInteract> ().SetVariables (null, false);
		}
	}

	public override void ChangeState(){
        GetComponent<AudioSource>().Play();
		RpcChangePosition (!turned);
	}

	[ClientRpc]
	void RpcChangePosition(bool _turned){
		StartCoroutine (CountDown ());
		turned = _turned;
		if (turned) {
			myAnim ["LeverFinal"].time = myAnim ["LeverFinal"].length;
			myAnim ["LeverFinal"].speed = -1f;
			myAnim.Play ("LeverFinal");
		} else {
			myAnim ["LeverFinal"].speed = 1f;
			myAnim.Play ("LeverFinal");
		}
		action.Invoke ();
	}

	IEnumerator CountDown(){
		changeMaterial (usedMaterial);
		usable = false;
		yield return new WaitForSeconds (timeToWait);
		usable = true;
		changeMaterial (originalMaterial);
	}

	void changeMaterial(Material input){
		Material[] materials = new Material[2];
		materials [0] = input;
		materials [1] = input;
		this.GetComponent<Renderer> ().materials = materials;
	}
}
