﻿using UnityEngine;
using System.Collections;

public class InteractableObject :  redblueObject{

	protected bool usable = true;

	public bool canUse(){
		return usable;
	}

	public virtual void ChangeState(){
		Debug.Log ("Override me!");
	}
}
