﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PressurePlate : redblueObject {

	[Header("PressurePlate")]
	Vector3 originalPos;
	Vector3 endingPos;
	int playerStepping;
	int prevPlayerstepping = 0;

	bool firstTimeFalling = true;

	[SerializeField] NoParamAction ONAction;
	[SerializeField] NoParamAction OFFAction;

	float endingPosY = 0.135f;

	void Start(){
		originalPos = this.transform.localPosition;
		endingPos = new Vector3 (0f, endingPosY, 0f);
	}

	void OnCollisionEnter(Collision other){
		if (!GetComponent<AudioSource> ().isPlaying) {
			GetComponent<AudioSource> ().Play ();
		}
		playerStepping++;
		if (playerStepping > 0 && prevPlayerstepping < 1) {
			StopAllCoroutines ();
			StartCoroutine (fallDownAsCoroutine ());
			if (hasAuthority) {
				CmdSwitch (true);
			}
			prevPlayerstepping = playerStepping;
		}
	}

	void OnCollisionExit(Collision other){
		playerStepping--;
		if (playerStepping == 0) {
			StopAllCoroutines ();
			StartCoroutine (moveBackAsCoroutine ());
			CmdSwitch (false);
			prevPlayerstepping = playerStepping;
		}
	}

	IEnumerator fallDownAsCoroutine(){
		while (Vector3.Distance (this.transform.localPosition, endingPos) > 0.01f) {
			this.transform.localPosition = Vector3.MoveTowards (this.transform.localPosition, endingPos, Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
	}

	IEnumerator moveBackAsCoroutine(){
		while (Vector3.Distance (this.transform.localPosition, originalPos) > 0.01f) {
			this.transform.localPosition = Vector3.MoveTowards (this.transform.localPosition, originalPos, Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
	}

	[Command]
	public void CmdSwitch(bool flag){
		RpcSwitch (flag);
	}

	[ClientRpc]
	void RpcSwitch(bool flag){
		if (flag) {
			ONAction.Invoke ();
		} else {
			OFFAction.Invoke ();
		}
	}
}
