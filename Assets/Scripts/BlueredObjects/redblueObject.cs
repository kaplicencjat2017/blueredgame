﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class redblueObject : NetworkBehaviour {
	[Header("redblueObject")]
	public bool pingable = true;
	public bool pingColor;
	public Color color;

	protected void OnMouseOver(){
		if (pingable) {
			if (Input.GetMouseButtonDown(1)) {
				foreach (PlayerInteract pi in FindObjectsOfType<PlayerInteract> ()) {
					if (pingColor) {
						pi.PingOnAll (this.gameObject.transform.position, color);
					} else {
						pi.PingOnAll (this.gameObject.transform.position);
					}
				}
			}
		}
	}

	public void OnMouseOverChild(){
		OnMouseOver ();
	}
}
