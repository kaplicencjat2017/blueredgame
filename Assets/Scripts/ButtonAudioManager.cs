﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAudioManager : MonoBehaviour{
	
    AudioSource AudioSource;
    public AudioClip HoverAudioClip;
    public AudioClip ClickAudioClip;

    public void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }
    
	public void PlayHover()
    {
        AudioSource.PlayOneShot(HoverAudioClip);
    }

    public void PlayClick()
    {
        AudioSource.PlayOneShot(ClickAudioClip);
    }
}
