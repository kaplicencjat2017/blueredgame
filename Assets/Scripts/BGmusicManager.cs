﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class BGmusicManager : MonoBehaviour
{

    public List<AudioClip> AudioClips = new List<AudioClip>();
    private AudioSource audioSource;
    public int LevelCounter = 0;

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("BGmusic");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        audioSource = GetComponent<AudioSource>();
        if (LevelCounter >= 3)
        {
            StartCoroutine(PlayNextSong());
        }


    }

	void OnEnable(){
		//Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable(){
		//Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode){
		LevelCounter++;
		if (LevelCounter >= 3) {
			StartCoroutine (PlayNextSong ());
		}
	}


    IEnumerator PlayNextSong()
    {
        for (int i = 0; i < 20; i++)
        {
            audioSource.volume -= 0.005f;
            yield return new WaitForSeconds(0.1f);
        }
        if (LevelCounter >= AudioClips.Count) LevelCounter = 3;
        audioSource.clip = AudioClips[LevelCounter];
        audioSource.Play();

        for (int i = 0; i < 20; i++)
        {
            audioSource.volume += 0.005f;
            yield return new WaitForSeconds(0.1f);
        }


    }
}

