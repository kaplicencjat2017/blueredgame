﻿using UnityEngine;
using System.Collections;

public class TestingPickAbleObject : MonoBehaviour {

	public Transform testPickPos;
	public Camera myCamera;

	void Update(){
		handlePickAbleObject ();
	}

	void handlePickAbleObject(){
		float distance = 2;
		testPickPos.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
		testPickPos.position = myCamera.ScreenToWorldPoint(testPickPos.position);
	}
}
